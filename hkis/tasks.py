"""Run using:
./manage.py correction_bot
"""

import asyncio
from functools import partial
from random import choice
from pathlib import Path
import os
import tempfile
import shlex
from subprocess import Popen, PIPE, run, STDOUT, TimeoutExpired, DEVNULL
from logging import getLogger

from celery import Celery

app = Celery("hackinscience_org")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()

logger = getLogger(__name__)

FIREJAIL_OPTIONS = [
    "--quiet",
    "--net=none",
    "--shell=none",
    "--x11=none",
    "--protocol=inet",
    "--private-dev",
    "--private-tmp",
    "--caps.drop=all",
    "--noprofile",
    "--nonewprivs",
    "--nosound",
    "--no3d",
    "--nogroups",
    "--noroot",
    "--seccomp",
    "--rlimit-fsize=16777216",  # 32768 is enough for Python exercises.
    "--rlimit-nofile=100",      # but cc needs way more (rust exercises).
    "--rlimit-nproc=2000",
    "--rlimit-cpu=20",
    "--rlimit-as=1610612736",  # 1.5GB. correction_helper will cap
    # student code at 1GB, leaving some space for check.py to report
    # errors.
    "--blacklist=/var",
    "--blacklist=/sys",
    "--blacklist=/boot",
]


def congrats(language):
    """Generates a congratulation sentence."""
    return (
        choice(
            {
                "en": [
                    "Congrats",
                    "Nice job",
                    "Well done",
                    "Spot on",
                    "Bravo",
                    "Nice",
                    "Good",
                ],
                "fr": [
                    "Bravo",
                    "Bien joué",
                    "Super",
                    "Excellent",
                    "Joli",
                ],
            }[language]
        )
        + choice(
            {
                "en": ["! ", "!! ", "!!! ", "! ! "],
                "fr": [" ! ", " !! ", " !!! ", " ! ! "],
            }[language]
        )
        + choice(
            {
                "en": [
                    "Your exercise is OK.",
                    "Right answer.",
                    "Good answer.",
                    "Correct answer.",
                    "Looks good to me!",
                    "Your answer is right.",
                    "Your answer is correct.",
                ],
                "fr": [
                    "C'est juste.",
                    "Bonne réponse.",
                    "Correct.",
                    "Ça me semble bon.",
                    "C'est la bonne réponse.",
                    "Excellente réponse.",
                ],
            }[language]
        )
    )


def run_in_jail(cmd, files: dict, env: dict = None):
    """Run cmd in a temporary directory, populated with the given files,
    and the given env."""
    with tempfile.TemporaryDirectory(prefix="hkis") as tmpdir:
        for file_name, file_content in files.items():
            (Path(tmpdir) / file_name).write_text(file_content, encoding="UTF-8")
        firejail_env = os.environ.copy()
        if env:
            firejail_env.update(env)
        argv = ["firejail"] + FIREJAIL_OPTIONS + ["--private=" + tmpdir] + cmd
        logger.debug("Running: %s", shlex.join(argv))
        jailed_proc = Popen(  # pylint: disable=consider-using-with
            argv,
            stdin=DEVNULL,
            stdout=PIPE,
            stderr=STDOUT,
            cwd=tmpdir,
            env=firejail_env,
        )
        try:
            stdout = (
                jailed_proc.communicate(timeout=40)[0]
                .decode("UTF-8", "backslashreplace")
                .replace("\u0000", r"\x00")
                .replace(  # Simplify tracebacks by hiding the temporary directory
                    'File "' + os.path.expanduser("~/"), 'File "'
                )
            )[:65_536]
            if jailed_proc.returncode == 0:
                return True, stdout
            if jailed_proc.returncode == 255:
                return False, "Checker timed out, look for infinite loops maybe?"
            return False, stdout
        except TimeoutExpired:
            jailed_proc.kill()
            jailed_proc.wait()
            return False, "Checker timed out."
        except MemoryError:
            return False, "Not enough memory to run your code."


@app.task
def check_answer_task(answer: dict):
    """Executed on Celery workers.
    answer should contain: check, source_code, and language.
    """
    logger.debug("Checking an answer.")
    success, message = run_in_jail(
        ["python3", "-u", "./check.py"],
        files={
            "check.py": answer["check"],
            "solution": answer["source_code"],
        },
        env={"LANGUAGE": answer["language"]} if "language" in answer else {},
    )
    if success and not message:
        message = congrats(answer.get("language", "en"))
    return success, message


async def check_answer(answer: dict):
    """Executed Django side.

    TODO with Celery 5: should no longer need run_in_executor.
    """

    def sync_celery_check_answer(answer: dict):
        return check_answer_task.apply_async((answer,), expires=60).get()

    return await asyncio.get_running_loop().run_in_executor(
        None, partial(sync_celery_check_answer, answer=answer)
    )
