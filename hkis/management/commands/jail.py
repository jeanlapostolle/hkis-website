from django.core.management.base import BaseCommand
import subprocess
from time import perf_counter
from hkis.tasks import run_in_jail
import logging

class Command(BaseCommand):
    help = "Run a Python file, sandboxed as it would be by a correction bot."

    def add_arguments(self, parser):
        parser.add_argument("file", type=str)
        parser.add_argument("--verbose", action="store_true")
        parser.add_argument("--timeit", action="store_true")

    def handle(self, file, verbose=False, timeit=False, **options):
        if verbose:
            logging.getLogger('hkis.tasks').level = logging.DEBUG
            logging.getLogger('').addHandler(logging.StreamHandler())
        with open(file, encoding="UTF-8") as ifile:
            begin = perf_counter()
            _success, stdout = run_in_jail(
                ["python3", "-u", "file.py"], {"file.py": ifile.read()}
            )
            end = perf_counter()
        print(stdout)
        if timeit and verbose:
            print(f"Ran in {end-begin:.2f}s")
